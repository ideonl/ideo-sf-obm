/**
 * @description Mocking class to mock a response to a outboundMessage;
 * @param HttpRequest object.
 */
@isTest
public class OutboundMessageMock implements HttpCalloutMock {
    /**
     * @description Implements the interface method for mocking a HTTP response.
     * @param req HTTPRequest object.
     * @return a correct SOAP response with a 200 status code.
     */
    public HTTPResponse respond(HTTPRequest req) {        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xsi:schemaLocation="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><sf:notifications xmlns:sf="http://soap.sforce.com/2005/09/outbound"><sf:OrganizationId>00D1l0000000O72EAE</sf:OrganizationId><sf:ActionId>m021l000000CnYkAAK</sf:ActionId><sf:SessionId>00D1l0000000O72!AR8AQH0imc2cqgUMLA0hmkYkznViTmz2hI_RcYAUF6aQTHZx9gZmAHrOU5VhvG4VCPqkKgvS2ssSNjUnFuIOYROmY6lTx_0b</sf:SessionId><sf:EnterpriseUrl>https://pidpa--pidpao.my.salesforce.com/services/Soap/u/51.0/00D1l0000000O72EAE</sf:EnterpriseUrl><sf:PartnerUrl>https://pidpa--pidpao.my.salesforce.com/services/Soap/c/51.0/00D1l0000000O72EAE</sf:PartnerUrl><sf:Notification><sf:Id>a0r1l00000D0KU4AAN</sf:Id><sf:sObject xmlns="urn:sobject.enterprise.soap.sforce.com"><Id>08p1l000000UPz8AAG</Id><ParentRecordId>1WL1l000000QiQQGA0</ParentRecordId><SAPOperationNumber__c>0010</SAPOperationNumber__c><SAPOrderNumber__c>50213127</SAPOrderNumber__c><SRExternalID__c>,</SRExternalID__c><SAPAppointmentStatus__c>KLAF</SAPAppointmentStatus__c><ArrivalWindowStartTime>2021-02-03T06:30:00.000Z</ArrivalWindowStartTime><ArrivalWindowEndTime>2021-02-03T11:30:00.000Z</ArrivalWindowEndTime><IsCustomerAppointment__c>true</IsCustomerAppointment__c><IsSAPFillPERNR__c>false</IsSAPFillPERNR__c></sf:sObject></sf:Notification></sf:notifications></soapenv:Body></soapenv:Envelope>');
        res.setStatusCode(200);
        System.debug(LoggingLevel.INFO, 'Http mock outbound message: ' + res);
        return res;
    }
}