/**
 * @description Mocking class to mock a thrown exception by a http call;
 * @param HttpRequest object.
 */
@IsTest
Public class FatalErrorMock implements HttpCalloutMock {
     /**
     * @description Implements the interface method for mocking a HTTP response.
     * @param req HTTPRequest object.
     * @return throws an Error instead of returning a response
     */
    public HttpResponse respond(HttpRequest req) {
        CalloutException e = (CalloutException)CalloutException.class.newInstance();
        e.setMessage('Unauthorized endpoint, please check Setup->Security->Remote site settings.');
        throw e;
    }
}