@isTest
public class MessageTriggerHandlerTest {

    @IsTest
    public static void sendSingularMessageTest() {
        TestObject__c obj = new TestObject__c(Name='Test-Name', IdeoOBM__TestDate__c=System.today());
        insert obj;

        Outbound_Message__c msg = New Outbound_Message__c();
        msg.Name = obj.Id;
        msg.Destination__c = 'TestDestination';
        msg.sObjectId__c = obj.Id;      
        
        Test.setMock(HttpCalloutMock.class, new OutboundMessageMock());
        
        Test.startTest();    
        insert msg;
        Test.stopTest(); 
        
        Outbound_Message__c msgRetrieved = [SELECT id, Status__c, ResponseStatusCode__c from Outbound_Message__c WHERE id = :msg.Id];
        System.assertEquals('Ready', msgRetrieved.IdeoOBM__Status__c, 'msgRetrieved.IdeoOBM__Status__c != \'Ready\'');
        System.assertEquals(null, msgRetrieved.ResponseStatusCode__c, 'msgRetrieved.ResponseStatusCode__c != null');
    }
    
    @IsTest
    public static void sendBulkMessages() {
        
        TestObject__c[] objs = new List<TestObject__c>();
        
        for (Integer i = 0; i < 200; i++) {
            TestObject__c obj = new TestObject__c(Name='Test-Name-'+i, IdeoOBM__TestDate__c=System.today());
            objs.add(obj);
        }

        insert objs;
        Outbound_Message__c[] msgs = New List<Outbound_Message__c>();
        
        for (TestObject__c obj : objs) {
            Outbound_Message__c msg = New Outbound_Message__c();
        	msg.Name = obj.Id;
        	msg.Destination__c = 'TestDestination';
        	msg.sObjectId__c = obj.Id;
            msgs.add(msg);
        }        
        
        Test.setMock(HttpCalloutMock.class, new OutboundMessageMock());
        
        Test.startTest();    
        insert msgs;
        Test.stopTest();
        
        Map<Id,SObject> msgsMap = new Map<Id,SObject>(msgs);
        
        Outbound_Message__c[] msgsRetrieved = [SELECT id, Status__c, ResponseStatusCode__c from Outbound_Message__c WHERE id in :msgsMap.keyset()];
        
        System.assertEquals(200, msgsRetrieved.size(), 'A different number than 200 messages were inserted, namely: ' + msgsRetrieved.size());
    } 
}