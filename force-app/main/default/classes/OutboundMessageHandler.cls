/**
 * @author Maarten Brands, Ideo
 * @date 12-01-2021
 * @description Handler for the trigger of the object Outbound_Message.
**/

public with sharing class OutboundMessageHandler {
        
    public static final String PARAMETER_OUTBOUND_INTERFACE_ENABLED = 'Enabled';
    public static final String PARAMETER_OUTBOUND_INTERFACE_QUEUE_SIZE = 'QueueSize';
    public static final String PARAMETER_OUTBOUND_INTERFACE_TIMEOUT = 'Timeout';

    /**
     * @description Is the Outbound Interface enabled? It can't be disabled as the backend f.e. is not available.
     * @param parameter the parameter to retrieve from the metadata Parameter__mdt.
     * @return Interface enabled
     **/
    public static String getParameter(String parameter) {
        Parameter__mdt mdtParameter = [SELECT Value__c FROM Parameter__mdt WHERE DeveloperName =: parameter WITH SECURITY_ENFORCED];
        System.debug(LoggingLevel.INFO, 'Parameter Enabled value: ' + mdtParameter.Value__c);
        return mdtParameter.Value__c;
    }
    
    /**
     * @description Send out the given outbound message
     * @param messageId Id of the outbound message that needs to be sent
     **/
    @future (callout=true)
    public static void sendMessage(Id messageId) {

        Outbound_Message__c message = [SELECT Id, Destination__c, sObjectId__c FROM Outbound_Message__c WHERE Id =: messageId WITH SECURITY_ENFORCED];
        
        try {
            Destination__mdt destination = [SELECT Id, FieldSet__c, ObjectName__c, NamedCredential__c FROM Destination__mdt WHERE DeveloperName =: message.Destination__c WITH SECURITY_ENFORCED];
            Integer timeout = Integer.valueOf(getParameter(PARAMETER_OUTBOUND_INTERFACE_TIMEOUT));
            
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            
            System.debug(LoggingLevel.INFO, 'destination: ' + destination.NamedCredential__c);
            // Setup the HTTP headers
            request.setEndpoint('callout:' + destination.NamedCredential__c);
            request.setMethod('POST');
            request.setHeader('Content-Type', 'text/xml');
            request.setTimeout(timeout);
            
            // Create the Message which should be sent
            String content = buildContent(message, destination);
            
            request.setBody(content);
            message.RequestMessage__c = content;
            System.debug(LoggingLevel.INFO, 'Http request: ' + request);

            // Send the SOAP message as setup
            HttpResponse response = http.send(request);
            System.debug(LoggingLevel.INFO, 'Http response: ' + response);
            
            // Parse the SOAP response
            if (response.getStatusCode() == 200) {
                message.Status__c = 'Sent';
            } else {
                message.Status__c = 'Retry';
            }
            
            // Retrieve additional data from the response
            message.ResponseStatusCode__c = response.getStatusCode();
            message.ResponseMessage__c = response.getBody();
            
            update message;           
            
            System.debug(LoggingLevel.INFO, 'Message response code: ' + response.getStatusCode());
            System.debug(LoggingLevel.INFO, 'Message response updated: ' + message);
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR, 'Error on sending message: ' + ex.getMessage());
            System.debug(LoggingLevel.ERROR, 'exception: ' + ex);
            System.debug(LoggingLevel.ERROR, 'getType: '+ ex.getTypeName());       
            System.debug(LoggingLevel.ERROR, 'Cause: ' + ex.getCause());
            
            if (ex.getMessage() == 'Unable to tunnel through proxy. Proxy returns "HTTP/1.1 503 Service Unavailable"'){
                message.ResponseStatusCode__c = 503; // make http and https both set status code as 503
                message.Status__c = 'Retry';
            } else {
            	message.ResponseStatusCode__c = -1;
                message.Status__c = 'Error';
        	}
            message.ResponseMessage__c = 'Message: ' + ex.getMessage() + '; Stacktrace: ' + ex.getStackTraceString();

        	update message;  
            System.debug(LoggingLevel.ERROR, 'Error occured with sending message: ' + ex.getMessage());          
        }
    }
    
    /**
     * @description Build the outbound message content
     * @param message Outbound message object which contains the object information which should be sent oud
     * @param destination Destination object which contains the configuration to send the message
     * @return Message content
     **/
    @TestVisible private static String buildContent(Outbound_Message__c message, Destination__mdt destination) {

        // Setup SOAP Message
        DOM.Document doc = new DOM.Document();
        String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
        String xsi = 'http://www.w3.org/2001/XMLSchema-instance';        
        DOM.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');
        envelope.setNamespace('xsi', xsi);
        envelope.setAttributeNS('schemaLocation', soapNS, xsi, null);
        
        // Build body of the SOAP message
        DOM.XmlNode body = envelope.addChildElement('Body', soapNS, null);
        buildBodyMessage(body, message, destination);
        System.debug(LoggingLevel.INFO, 'Content to send: ' + doc.toXmlString());
        return doc.toXmlString();
    }
    
    /**
     * @description Build the body for the SOAP Message by configuration and data
     * @param body the DOM.XmlNode resembling the body to be filled in
     * @param message Contains the object id to retrieve the information from
     * @param destination contains information object name and fieldset 
     **/
    private static void buildBodyMessage(DOM.XmlNode body, Outbound_Message__c message, Destination__mdt destination) {

		// Retrieve the data setup from the configuration and the actual data from the database
        Schema.FieldSet fieldSet = readFieldSet(destination.FieldSet__c, destination.ObjectName__c);
        System.debug(LoggingLevel.INFO, 'Fieldset: ' + fieldSet);
        
        String query = 'SELECT Id, ' + convertFieldSetToCSV(fieldSet) + ' FROM ' + String.escapeSingleQuotes(destination.ObjectName__c) + ' WHERE Id = \'' + String.escapeSingleQuotes(message.sObjectId__c) + '\'';
        
        System.debug(LoggingLevel.INFO, 'Query build: ' + query); 
        
        // Enforce FLS Settings salesforce.
        SObjectAccessDecision securityDecision = Security.stripInaccessible(AccessType.READABLE, Database.query(query));
        sObject data = securityDecision.getRecords()[0];
      
        System.debug(LoggingLevel.DEBUG, securityDecision.getRemovedFields().size() + ' fields are not included because of field level security settings');  
        
        // Build the body message
        String nsBody = 'http://soap.sforce.com/2005/09/outbound';
        String prefixBody = 'sf';
        DOM.XmlNode nodeNotifications = body.addChildElement('notifications', nsBody, prefixBody);
        DOM.XmlNode nodeOrganizationId = nodeNotifications.addChildElement('OrganizationId', nsBody, prefixBody);
        nodeOrganizationId.addTextNode(UserInfo.getOrganizationId());
        DOM.XmlNode nodeActionId = nodeNotifications.addChildElement('ActionId', nsBody, prefixBody);
        nodeActionId.addTextNode(destination.Id);
        DOM.XmlNode nodeSessionId = nodeNotifications.addChildElement('SessionId', nsBody, prefixBody);
        nodeSessionId.addTextNode(UserInfo.getSessionId() != null ? UserInfo.getSessionId() : '');
        DOM.XmlNode nodeEnterpriseUrl = nodeNotifications.addChildElement('EnterpriseUrl', nsBody, prefixBody);
        nodeEnterpriseUrl.addTextNode(URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/51.0/' + UserInfo.getOrganizationId());
        DOM.XmlNode nodePartnerUrl = nodeNotifications.addChildElement('PartnerUrl', nsBody, prefixBody);
        nodePartnerUrl.addTextNode(URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/c/51.0/' + UserInfo.getOrganizationId());
        DOM.XmlNode nodeNotification = nodeNotifications.addChildElement('Notification', nsBody, prefixBody);
        DOM.XmlNode nodeId = nodeNotification.addChildElement('Id', nsBody, prefixBody);
        nodeId.addTextNode(message.Id);
        
        // Build the sObject message
        String nsObject = 'urn:sobject.enterprise.soap.sforce.com';
        DOM.XmlNode nodeObject = nodeNotification.addChildElement(prefixBody + ':sObject', nsObject, '');
        DOM.XmlNode nodeElement = nodeObject.addChildElement('Id', nsObject, '');
        nodeElement.addTextNode((String)data.get('Id'));
        
        // Loop over the fieldset fields and add them to the list
        // Via this way the order of the fieldset will be used and they will be sent in the right order as the WSDL will expect it
        for (Schema.FieldSetMember fieldSetMember : fieldSet.getFields()) {
            Object value = data.get(fieldSetMember.getFieldPath());            
            if (value != null) {

                nodeElement = nodeObject.addChildElement(fieldSetMember.getFieldPath(), nsObject, '');
                
                // Only with datetime object type format it to another format, if it's empty, do not add into the message
                if (fieldSetMember.getType() == Schema.DisplayType.DATETIME) {
                    Datetime dtValue = (Datetime) value;
                    nodeElement.addTextNode((String)dtValue.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSXXX')); // format instead of formatGMT 
                } else {
                    nodeElement.addTextNode(value.toString());
                }
        	}
        }
    }
        
    /**
     * @description Read fieldset from the database and return the fields to query with
     * @param name Name of the fieldset which should be retrieved
     * @param objectName Name of the object type where the fieldset should be retrieved from
     * @return FieldSet Schema
     **/
    private static Schema.FieldSet readFieldSet(String name, String objectName) {
        Map<String, Schema.SObjectType> globalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType sobjectType = globalDescribeMap.get(objectName);
        Schema.DescribeSObjectResult describeSObjectType = sobjectType.getDescribe();
        return describeSObjectType.FieldSets.getMap().get(name);		
    }
    
    /**
     * @description Convert the FieldSet schema to a CSV value
     * @param fieldSet The FieldSet Schema
     * @return FieldSet in CSV
     **/
    private static String convertFieldSetToCSV(Schema.FieldSet fieldSet) {
        
        String fields = '';
        
        // Loop over the fieldset fields and add them to the list
        for (Schema.FieldSetMember fieldSetMember : fieldSet.getFields()) {
            fields += fieldSetMember.getFieldPath() + ',';
            system.debug(LoggingLevel.INFO, 'API Name ====>' + fieldSetMember.getFieldPath()); //api name
            system.debug(LoggingLevel.INFO, 'Label ====>' + fieldSetMember.getLabel());
            system.debug(LoggingLevel.INFO, 'Required ====>' + fieldSetMember.getRequired());
            system.debug(LoggingLevel.INFO, 'DbRequired ====>' + fieldSetMember.getDbRequired());
            system.debug(LoggingLevel.INFO, 'Type ====>' + fieldSetMember.getType());   //type - STRING,PICKLIST
        }
        
        return fields.substringBeforeLast(','); 
    }
}