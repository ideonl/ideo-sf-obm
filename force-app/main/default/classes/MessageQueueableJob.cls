/**
 * @description MessageQueueableJob, the queue which contains the messages that still have to be sent or need a retry.
 */
public class MessageQueueableJob implements Queueable, Database.AllowsCallouts {

    private List<Outbound_Message__c> messages = null;
    
    /**
     * @description The constructor for the MessageQueueableJob, containing a list of messages
     * @param messages the list of Outbound_Message__c objects to be send/checked.
     */
    public MessageQueueableJob(List<Outbound_Message__c> messages) {
        this.messages = messages;
    }

	/**
     * @description execute method which needs to be implemented for the Queueable interface
     * Tries to send a batch of messages at once or requeue's them to be send later on.
     * @param context the QueueableContext required by this method.
     */
    public void execute(QueueableContext context) {
        if (!messages.isEmpty()) {
            System.debug(LoggingLevel.INFO, 'Is this executed from a queue? ' + System.isQueueable());
            
            List<Outbound_Message__c> newMessages = new List<Outbound_Message__c>();
        	Integer queueSize = Integer.valueOf(OutboundMessageHandler.getParameter(OutboundMessageHandler.PARAMETER_OUTBOUND_INTERFACE_QUEUE_SIZE));            
            
            for(Integer i = 0; i < messages.size(); i++) {
                if (i < queueSize) {
					OutboundMessageHandler.sendMessage(messages[i].Id);
                } else {
                    newMessages.add(messages[i]);
                }
            }
            
            if (!newMessages.isEmpty() && !Test.isRunningTest()) {
                System.enqueueJob(new MessageQueueableJob(newMessages));
            }
        }        
    }
}