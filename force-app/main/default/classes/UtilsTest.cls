/**
 * @description Test class for the functionality in the Utils collection of methods.
 */
@IsTest
public class UtilsTest {

    /**
     * @description TestMethod for getCreateableFieldsSOQL tests if code correctly gives null when accessed with null
     */
    @isTest
    static void getCreatableFieldsSOQL(){
        
        String nullString = null;
        Test.startTest();
        String output = Utils.getCreatableFieldsSOQL('Account', '');
        String output2 = Utils.getCreatableFieldsSOQL('Contact', nullString);
        Test.stopTest();
        
        System.assertEquals(null, output, 'Null not given as response for Utils.getCreatableFieldsSOQL(\'Account\', \'\');');
        System.assertEquals(null, output2, 'Null not given as response for Utils.getCreatableFieldsSOQL(\'Contact\', nullString);');
    }
    
    /**
     * @description Checks if the code correctly finds standard fields for the Account object.
     */
    @isTest
    static void getCreatableFieldsSOQLAccountHappyFlow() {
        
        Account testAccount = new Account(Name='TestAccount');
        insert testAccount; 
        String whereClause = 'Name = TestAccount';
        
        Test.startTest();
        String query = Utils.getCreatableFieldsSOQL('Account', whereClause);
        Test.stopTest();      
        
        System.assert(query.contains('BillingStreet'), 'query does not contain BillingStreet');
        System.assert(query.contains('Name'), 'query does not contain Name');
        System.assert(query.contains('Rating'), 'query does not contain Rating');
        
        String endOfString = query.substring(query.length() - whereClause.length());
        System.assert(endOfString == whereClause, 'incorrect whereClaus, endOfString:' + endOfString + '||whereClause:' + whereClause);
    }
}