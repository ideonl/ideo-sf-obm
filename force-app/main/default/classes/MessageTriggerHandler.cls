/**
 * @author Maarten Brands, Ideo
 * @date 12-01-2021
 * @description Handler for the trigger of the object Oubound Message.
**/
public class MessageTriggerHandler {

    /**
     * @description Handler after Outbound_Message is inserted
     * @param messages The list of Message objects which are inserted
     **/
    public static void afterInsert(List<Outbound_Message__c> messages) {
        handleOutboundMessages(messages);
    }

    /**
     * @description Handler after Outbound_Message is updated
     * @param messages The list of Message objects which are updated
     **/
    public static void afterUpdate(List<Outbound_Message__c> messages) {
        handleOutboundMessages(messages);
    }

    /**
     * @description Handler for sending the Outbound Message(s)
     * @param messages A list of inserted/updated outbound messages to send
     **/
    private static void handleOutboundMessages(List<Outbound_Message__c> messages) {

        // Verify if the outbound queue is not requested by itself, this gives non-preferred behaviour and errors
        if (!System.isFuture() && !System.isQueueable() && OutboundMessageHandler.getParameter(OutboundMessageHandler.PARAMETER_OUTBOUND_INTERFACE_ENABLED) == 'TRUE') {
            List<Outbound_Message__c> messagesForQueue = new List<Outbound_Message__c>();

            for (Integer i = 0; i < messages.size(); i++) {
                System.debug(LoggingLevel.INFO, 'Message (' + i + '): ' + messages[i].Id);
                
                // Only send the message when it's set as Ready, with other statuses it shouldn't sent
                if (messages[i].Status__c == 'Ready') {
                	messagesForQueue.add(messages[i]);
                }
            }

            // When there are messages left over to be sent add them to the queue job and sent them
            if (!messagesForQueue.isEmpty()) {
                ID jobId = System.enqueueJob(new MessageQueueableJob(messagesForQueue));
                System.debug(LoggingLevel.INFO, 'Job queue created with ID: ' + jobId);
            }
        }
    }
}