@isTest
public class OutboundMessageHandlerTest {

    @isTest
    public static void sendSingularMessageTest() {
        TestObject__c obj = new TestObject__c(Name='Test-Name', IdeoOBM__TestDate__c=System.now());
        insert obj;

        Outbound_Message__c msg = New Outbound_Message__c();
        msg.Name = obj.Id;
        msg.Destination__c = 'TestDestination';
        msg.sObjectId__c = obj.Id;      
        
        Test.setMock(HttpCalloutMock.class, new OutboundMessageMock());
        
        Test.startTest();    
        insert msg;
        OutboundMessageHandler.sendMessage(msg.Id);
        Test.stopTest();
        
        Outbound_Message__c msgRetrieved = [SELECT id, Status__c, ResponseStatusCode__c, ResponseMessage__c from Outbound_Message__c WHERE id = :msg.Id];
        System.assertEquals('Sent', msgRetrieved.IdeoOBM__Status__c, 'IdeoOBM__Statuc__c != \'Sent\''); 
        System.assertEquals(200, msgRetrieved.ResponseStatusCode__c, 'ResponseStatusCode__c != 200');
        System.assertEquals('<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xsi:schemaLocation="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><sf:notifications xmlns:sf="http://soap.sforce.com/2005/09/outbound"><sf:OrganizationId>00D1l0000000O72EAE</sf:OrganizationId><sf:ActionId>m021l000000CnYkAAK</sf:ActionId><sf:SessionId>00D1l0000000O72!AR8AQH0imc2cqgUMLA0hmkYkznViTmz2hI_RcYAUF6aQTHZx9gZmAHrOU5VhvG4VCPqkKgvS2ssSNjUnFuIOYROmY6lTx_0b</sf:SessionId><sf:EnterpriseUrl>https://pidpa--pidpao.my.salesforce.com/services/Soap/u/51.0/00D1l0000000O72EAE</sf:EnterpriseUrl><sf:PartnerUrl>https://pidpa--pidpao.my.salesforce.com/services/Soap/c/51.0/00D1l0000000O72EAE</sf:PartnerUrl><sf:Notification><sf:Id>a0r1l00000D0KU4AAN</sf:Id><sf:sObject xmlns="urn:sobject.enterprise.soap.sforce.com"><Id>08p1l000000UPz8AAG</Id><ParentRecordId>1WL1l000000QiQQGA0</ParentRecordId><SAPOperationNumber__c>0010</SAPOperationNumber__c><SAPOrderNumber__c>50213127</SAPOrderNumber__c><SRExternalID__c>,</SRExternalID__c><SAPAppointmentStatus__c>KLAF</SAPAppointmentStatus__c><ArrivalWindowStartTime>2021-02-03T06:30:00.000Z</ArrivalWindowStartTime><ArrivalWindowEndTime>2021-02-03T11:30:00.000Z</ArrivalWindowEndTime><IsCustomerAppointment__c>true</IsCustomerAppointment__c><IsSAPFillPERNR__c>false</IsSAPFillPERNR__c></sf:sObject></sf:Notification></sf:notifications></soapenv:Body></soapenv:Envelope>', msgRetrieved.ResponseMessage__c, 'Soap response is not the same as in the Mock.');
        
    }
    
    @isTest   
    public static void sendSingularMessageError() {    
        TestObject__c obj = new TestObject__c(Name='Test-Name',IdeoOBM__TestDate__c=System.now());
        insert obj;

        Outbound_Message__c msg = New Outbound_Message__c();
        msg.Name = obj.Id;
        msg.Destination__c = 'TestDestination';
        msg.sObjectId__c = obj.Id;          
        
        Test.setMock(HttpCalloutMock.class, new HttpErrorCalloutMock());
        
        Test.startTest();    
        insert msg;
        OutboundMessageHandler.sendMessage(msg.Id);
        Test.stopTest();
        
        Outbound_Message__c msgRetrieved = [SELECT id, Status__c, ResponseStatusCode__c from Outbound_Message__c WHERE id = :msg.Id];

        System.assertEquals('Retry', msgRetrieved.IdeoOBM__Status__c, 'msgRetrieved.IdeoOBM__Status__c != \'Retry\''); //System will retry later.
        System.assertEquals(123, msgRetrieved.ResponseStatusCode__c, 'msgRetrieved.ResponseStatusCode__c != 123');
        System.debug(LoggingLevel.DEBUG, 'Status: '+ msgRetrieved.IdeoOBM__Status__c);
    }

    @isTest    
    public static void sendSingularMessageFatalError() {    
        TestObject__c obj = new TestObject__c(Name='Test-Name',IdeoOBM__TestDate__c=System.now());
        insert obj;

        Outbound_Message__c msg = New Outbound_Message__c();
        msg.Name = obj.Id;
        msg.Destination__c = 'TestDestination';
        msg.sObjectId__c = obj.Id;          
        
        Test.setMock(HttpCalloutMock.class, new FatalErrorMock());
        
        Test.startTest();    
        insert msg;
        OutboundMessageHandler.sendMessage(msg.Id);
        Test.stopTest();
        
        Outbound_Message__c msgRetrieved = [SELECT id, Status__c, ResponseStatusCode__c from Outbound_Message__c WHERE id = :msg.Id];
            
        System.assertEquals('Error', msgRetrieved.IdeoOBM__Status__c, 'msgRetrieved.IdeoOBM__Status__c != \'Error\'');
        System.assertEquals(-1, msgRetrieved.ResponseStatusCode__c, 'msgRetrieved.ResponseStatusCode__c != -1');
        System.debug(LoggingLevel.DEBUG, 'Status: '+ msgRetrieved.IdeoOBM__Status__c);
    }
    
/*
    @IsTest
    public static void buildContentTest() {  
        TestObject__c obj = new TestObject__c(Name='Test-Name',IdeoOBM__TestDate__c=System.today());
        insert obj;

        Outbound_Message__c msg = New Outbound_Message__c();
        msg.Name = obj.Id;
        msg.Destination__c = 'TestDestination';
        msg.sObjectId__c = obj.Id;
        
        Destination__mdt destination = [SELECT Id, FieldSet__c, ObjectName__c, NamedCredential__c FROM Destination__mdt WHERE DeveloperName =: msg.Destination__c];  
        
        String testXMLString = OutboundMessageHandler.buildContent(msg, destination);
    
    }
*/
}