/**
 * @author Maarten Brands, Ideo
 * @date 17-12-2020
 * @description Generic utitilies to support custom functionality.
**/
public with sharing class Utils{ 
 
    /**
     * @description Returns a dynamic SOQL statement for the whole object, includes only creatable fields since we will be inserting a cloned result of this query
     * @param objectName Name of the SalesForce object
     * @param whereClause Filter objects
     * @return SQL Query
     **/
    public static string getCreatableFieldsSOQL(String objectName, String whereClause) {
         
        String selects = '';
         
        if (whereClause == null || whereClause == '') { return null; }
         
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fieldMap != null) {

            // loop through all field tokens
            for (Schema.SObjectField fieldTokens : fieldMap.values()) {
                
                // describe each field
                Schema.DescribeFieldResult fieldDescription = fieldTokens.getDescribe();

                if (fieldDescription.isCreateable()) {
                    selectFields.add(fieldDescription.getName());
                }
            }
        }
         
        if (!selectFields.isEmpty()) {

            for (string s : selectFields) {
                selects += s + ',';
            }

            if (selects.endsWith(',')) { 
                selects = selects.substring(0, selects.lastIndexOf(',')); 
            }             
        }
        
        // System.debug(LoggingLevel.INFO, 'Query is: ' + 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause);  
        return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;         
    }
}