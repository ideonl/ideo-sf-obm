/**
 * @author Maarten Brands, Ideo
 * @date 12-01-2021
 * @description Trigger for object iomMessage.
**/
trigger iomMessageTrigger on iomMessage__c (before insert, after insert, after update) {
    
    // Logging information for limits
	System.debug('Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
    System.debug('Total Number of records that can be queried  in this Apex code context: ' +  Limits.getLimitDmlRows());
    System.debug('Total Number of DML statements allowed in this Apex code context: ' +  Limits.getLimitDmlStatements() );
    System.debug('Total Number of CPU usage time (in ms) allowed in this Apex code context: ' +  Limits.getLimitCpuTime());

    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
    		iomMessageTriggerHandler.afterInsert(Trigger.new);
        } else if (Trigger.isUpdate) {
    		iomMessageTriggerHandler.afterUpdate(Trigger.new);
        }
    }
}