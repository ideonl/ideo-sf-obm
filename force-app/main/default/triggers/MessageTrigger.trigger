/**
 * @author Maarten Brands, Ideo
 * @date 12-01-2021
 * @description Trigger for object Outbound_Message.
**/
trigger MessageTrigger on IdeoOBM__Outbound_Message__c (after insert, after update) {
    
    // Logging information for limits
	System.debug(LoggingLevel.INFO, 'Total Number of SOQL Queries allowed in this Apex code context: ' +  Limits.getLimitQueries());
    System.debug(LoggingLevel.INFO, 'Total Number of records that can be queried  in this Apex code context: ' +  Limits.getLimitDmlRows());
    System.debug(LoggingLevel.INFO, 'Total Number of DML statements allowed in this Apex code context: ' +  Limits.getLimitDmlStatements() );
    System.debug(LoggingLevel.INFO, 'Total Number of CPU usage time (in ms) allowed in this Apex code context: ' +  Limits.getLimitCpuTime());

    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
    		MessageTriggerHandler.afterInsert(Trigger.new);
        } else if (Trigger.isUpdate) {
    		MessageTriggerHandler.afterUpdate(Trigger.new);
        }
    }
}